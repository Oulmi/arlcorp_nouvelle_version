<?php

namespace App\Repository;

use App\Entity\Eleve;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Eleve|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eleve|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eleve[]    findAll()
 * @method Eleve[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EleveRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Eleve::class);
    }
//
//    /**
//     * @param $pseudo
//     * @return mixed
//     * This method select users with the same login than the $login send in the function
//     */
//    public function getUsersPerLogin($pseudo)
//    {
//        $entityManager = $this->getEntityManager();
//
//        $query = $entityManager->createQuery(
//            'SELECT u.id
//        FROM  App\Entity\Eleve u
//        WHERE u.login = :pseudo'
//        )->setParameter('pseudo', $pseudo);
//
//        // returns an array of Product objects
//        return $query->execute();
//    }


    public function getUserWithoutStage(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "
            SELECT eleve FROM App\Entity\Eleve eleve where eleve.stage = '0' and eleve.roles LIKE '%{%ROLE_USER%}'
            "
        );

        return $query->execute();
    }

    public function findAllUsersRoleProfessor(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "
            SELECT eleve FROM App\Entity\Eleve eleve where eleve.roles LIKE '%{%ROLE_PROFESSOR%}'
            "
        );

        return $query->execute();
    }

    public function findAllUsersRoleUser(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "
            SELECT eleve FROM App\Entity\Eleve eleve where eleve.roles LIKE '%{%ROLE_USER%}'
            "
        );

        return $query->execute();
    }


}
