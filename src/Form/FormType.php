<?php namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface  $builder, array $options)
    {
        $builder
            ->add('prenom', TextType::class)
            ->add('nom', TextType::class)
            ->add('pseudo', TextType::class)
            ->add('plainpassword', RepeatedType::class,
                array('type' => PasswordType::class, 'first_options' =>
                    array('label' => 'Mot de passe'), 'second_options' =>
                    array('label' => 'Confirmation du mot de passe'),))
            ->add('submit', SubmitType::class,
                ['label' => 'Envoyer', 'attr' => ['class' => 'btn-primary btn-block']]);
    }
}