<?php

namespace App\Controller;

use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\Entreprise;
use App\Entity\Eleve;
use App\Form\FormType;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Date;

class SecurityAdminController extends AbstractController
{

    //
    //Professeur//////////////////////////////////////////////////////////////////////////////////////////////////
    //

    /**
     * @Route("admin/liste/professeur", name="liste_professeur")
     */
    public function listeProfesseur()
    {
        //Doctrine communication avec la base de données -> comme des objets
        $entityManager = $this->getDoctrine()->getManager();
        //It's responsible for saving objects to, and fetching objects from, the database.

        $em = $entityManager->getRepository(Eleve::class)->findAllUsersRoleProfessor();

        foreach ($em as $p) {
            $p->getPrenom();
            $p->getNom();
            $p->getPseudo();
            $p->getMotDePasse();
        }

        $entityManager->flush();

        return $this->render('security_user/listeProfesseur.html.twig',
            array('em' => $em));
    }
    /**
     * @Route("admin/professeur/ajout", name="add_professeur")
     */
    public function ajoutProfesseur(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $prof = new Eleve();
        $prof->setRoles(["ROLE_PROFESSOR"]);
        $form = $this->createForm(FormType::class, $prof);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($prof, $prof->getPlainPassword());
            $prof->setMotDePasse($password);
            $em = $this->getDoctrine()->getManager();
            $prof->setStage("0");
            $em->persist($prof);
            $em->flush();
            $this->addFlash('success',
                'Le compte a bien été enregistré.');
            return $this->redirectToRoute('liste_professeur');
        }
        return $this->render('security_user/ajoutUser.html.twig',
            array('form' => $form->createView()));
    }


    //
    //Eleve//////////////////////////////////////////////////////////////////////////////////////////////////
    //

    /**
     * @Route("admin/liste/eleve", name="liste_eleve")
     */

    public function listeEleve(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $em = $entityManager->getRepository(Eleve::class)->findAllUsersRoleUser();
        foreach ($em as $p) {
            $p->getPrenom();
            $p->getNom();
            $p->getPseudo();
            $p->getMotDePasse();
        }
        $entityManager->flush();
        return $this->render('security_user/listeEleve.html.twig',
            array('em' => $em));
    }

    /**
     * @Route("admin/eleve/ajout", name="ajout_eleve")
     */
    public function ajoutEleve(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $eleve = new Eleve();
        $eleve->setRoles(["ROLE_USER"]);
        $form = $this->createFormBuilder($eleve)
            ->add('prenom', TextType::class, array('label' => "Prenom"))
            ->add('nom', TextType::class, array('label' => "Nom"))
            ->add('pseudo', TextType::class, array('label' => "Pseudo"))
            ->add('plainpassword', PasswordType::class, array('label' => "Mot de Passe :"))
            ->add('save', SubmitType::class, array('label' => "Sign in"))
            ->getForm();
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($eleve, $eleve->getPlainPassword());
            $eleve->setMotDePasse($password);
            $em = $this->getDoctrine()->getManager();
            $eleve->setStage("0");
            $em->persist($eleve);
            $em->flush();
            $this->addFlash('success',
                'Le compte a bien été enregistré.');
            return $this->redirectToRoute('liste_eleve');
        }
        return $this->render('security_user/ajoutUser.html.twig',
            array('form' => $form->createView()));
    }



    //
    //USER//////////////////////////////////////////////////////////////////////////////////////////////////
    //


    /**
     * @Route("admin/user/modifier/{id}", name="maj_user")
     */
    public
    function modifierEleve(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository(Eleve::class)->find($id);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id ' . $id
            );
        }
        $form = $this->createFormBuilder($user)
            ->add("nom", TextType::class)
            ->add("prenom", TextType::class)
            ->add("pseudo", TextType::class)
            ->add("plainpassword", PasswordType::class)
            ->add('roles', ChoiceType::class, array("choices" => ["ROLE_USER" => "ROLE_USER",
                "ROLE_PROFESSOR" => "ROLE_PROFESSOR", "ROLE_ADMIN" => "ROLE_ADMIN"]
            , 'expanded' => true, "multiple" => true))
            ->add('save', SubmitType::class, array('label' => 'Valider'))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            if (in_array('ROLE_PROFESSOR', $user->getRoles())) {
                return $this->redirectToRoute('liste_professeur');
            }
            else {
                return $this->redirectToRoute('liste_eleve');
            }
        }
        return $this->render('security_user/majUser.html.twig', array('form' => $form->createView()));

    }
        /**
     * @Route("admin/user/supprimer/{id}", name="eleve_delete")
     */
    public
    function deleteEleve($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $eleve = $entityManager->getRepository(Eleve::class)->find($id);

        if (!$eleve) {
            throw $this->createNotFoundException(
                'No user found for id ' . $id
            );
        }
        $entityManager->remove($eleve);
        $entityManager->flush();

        return $this->redirectToRoute('liste_eleve', [
            'id' => $eleve->getId()
        ]);
    }


    //
    //Entreprise//////////////////////////////////////////////////////////////////////////////////////////////////
    //

    /**
     * @Route("entreprise/liste", name="list_entreprise")
     */
    public function listeEntreprise(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $em = $entityManager->getRepository(Entreprise::class)->findAll();

        foreach ($em as $p) {
            $p->getNom();
            $p->getVille();
            $p->getCp();
            $p->getAdresse();
            $p->getMail();
            $p->getTel();
            $p->getActivite();
            $p->getActive();
        }
        $entityManager->flush();

        return $this->render('security_user/listEntreprise.html.twig',
            array('em' => $em,
                'p' => $p));
    }

    /**
     * @Route("entreprise/add", name="add_entreprise")
     */
    public function addEntreprise(Request $request)
    {
        $product = new Entreprise();
        $formBuild = $this->createFormBuilder($product)
            ->add("nom", TextType::class)
            ->add("ville", TextType::class)
            ->add("cp", TextType::class)
            ->add("adresse", TextType::class)
            ->add("mail", TextType::class)
            ->add("tel", TextType::class)
            ->add("activite", TextType::class)
            ->add('create', SubmitType::class, array("label" => "Ajouter l'Entreprise"));
        $form = $formBuild->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newEntreprise = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newEntreprise);
            $entityManager->flush();

            return $this->redirectToRoute("list_entreprise");
        }

        return $this->render('security_user/ajoutEntreprise.html.twig',
            array('form' => $form->createView(),
                'tab' => array('nom', 'ville', 'cp', 'adresse', 'mail', 'tel', 'activite')));
    }

    /**
     * @Route("entreprise/edit/{id}", name="update_entreprise")
     */
    public function updateEntreprise($id, Request $request)
    {
        $entrepriseToModif = $this->getDoctrine()->getRepository(Entreprise::class)->find($id);
        $form = $this->createFormBuilder($entrepriseToModif)
            ->add("nom", TextType::class)
            ->add("ville", TextType::class)
            ->add("cp", TextType::class)
            ->add("adresse", TextType::class)
            ->add("mail", TextType::class, array('label' => 'mail', 'required' => false))
            ->add("tel", TextType::class)
            ->add("activite", TextType::class)
            ->add('create', SubmitType::class, array("label" => "Valider"))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entrepriseToModif = $form->getData();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute("list_entreprise");
        }

        return $this->render('security_user/majEntreprise.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("entreprise/delete/{id}", name="delete_entreprise")
     */
    public function deleteEntreprise($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);
        if (!$entreprise) {
            throw $this->createNotFoundException(
                'Entreprise N°' . $id . ' Introuvable.'
            );
        } else {
            $entityManager->remove($entreprise);
            $entityManager->flush();
            return $this->redirectToRoute('list_entreprise');
        }
    }



}
