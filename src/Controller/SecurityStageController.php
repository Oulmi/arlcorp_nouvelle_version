<?php

namespace App\Controller;

use App\Entity\Eleve;
use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Professeur;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityStageController extends AbstractController
{
    /**
     * @Route("/stage/eleve_stage", name="eleve_stage")
     */
    public function eleve(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleves = $repository->getUserWithoutStage();
        $stage = new Stage();
        $userId = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $eleve = $this->getDoctrine()
            ->getRepository(Eleve::class)
            ->find($userId);

        # Build the form
        $formBuild = $this->createFormBuilder($stage)
            ->add('Eleve', EntityType::class, array('class' => Eleve::class, 'choice_label' => 'id', 'data' => $eleve,))
            ->add('Entreprise', EntityType::class, array('class' => Entreprise::class, 'choice_label' => 'nom',))
            ->add('Tuteur', TextType::class, array('label' => "Tuteur :"))
            ->add('date_debut', DateType::class, array(
                'widget' => 'choice',))
            ->add('date_fin', DateType::class, array(
                'widget' => 'choice',));

        # Getting the form
        $formBuild->add('create', SubmitType::class, array("label" => "Add stage"));
        $form = $formBuild->getForm();
        $form->handleRequest($request);

        # If the form is submitted
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $eleve->setStage("1");
            $entityManager->persist($data);
            $entityManager->persist($eleve);
            $entityManager->flush();
            return $this->redirectToRoute('eleve_stage');
        }

        $stageEleve = new Stage();

        try {
            $stageEleve = $entityManager->getRepository(Stage::class)->findByEleveId($userId);
        } catch (NonUniqueResultException $e) {
            $this->addFlash("danger", "Erreur !" . $e->getMessage());
        }
       // var_dump($userId);
        return $this->render('stage/index.html.twig', array('form' => $form->createView(), 'eleves' => $eleves, 'eleve' => $eleve, 'lestage' => $stageEleve));
    }


    /**
     * @Route("/eleve/stage/edit/{id}", name="stage_edit")
     */
    public function edit($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $eleve = $this->getDoctrine()
            ->getRepository(Eleve::class)
            ->find($userId);

        $stage = $entityManager->getRepository(Stage::class)->find($id);
        $stage->getEntreprise()->setActive("1");
        $form = $this->createFormBuilder($stage)
            ->add('Eleve', EntityType::class, array('class' => Eleve::class, 'choice_label' => 'id', 'data' => $eleve,))
            ->add('Entreprise', EntityType::class, array('class' => Entreprise::class, 'choice_label' => 'nom',))
            ->add('Tuteur', TextType::class, array('label' => "Tuteur :"))
            ->add('date_debut', DateType::class, array(
                'widget' => 'choice',))
            ->add('date_fin', DateType::class, array(
                'widget' => 'choice',))
            ->add('create', SubmitType::class, array("label" => "Edit"))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stage = $form->getData();
            $stage->getEntreprise()->setActive("0");
            $entityManager->persist($stage);
            $entityManager->flush();
            return $this->redirectToRoute('eleve_stage');
        }
        return $this->render('stage/edit.html.twig', array(
            'form' => $form->createView()));
    }


    /**
     * @Route("/stage/liste_stage", name="liste_stage")
     */
    public function listeStage()
    {
        $repo = $this->getDoctrine()->getRepository(Stage::class);
        $stage = $repo->findAll();
        return $this->render("stage/liste_stage.html.twig", array("stages" => $stage));
    }


    /**
     * @Route("/prof/stage/{id}", name="prof_stage2")
     */
    public function referent(Request $request,$id)
    {
        $stage =  $this->getDoctrine()->getRepository(Stage::class)->find($id);
        $userId = $this->getUser()->getId();
        $prof = $this->getDoctrine()
            ->getRepository(Eleve::class)
            ->find($userId);

        $entityManager = $this->getDoctrine()->getManager();

        $Stage = new Stage();
        $formBuild = $this->createFormBuilder($Stage)
            ->add('Eleve', EntityType::class, array('class' => Eleve::class, 'choice_label' => 'id', 'data' => $prof,));
        $formBuild->add('create', SubmitType::class, array("label" => "Valider"));

        $form = $formBuild->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $referent = $form->get('Eleve')->getData();
            $Lestage = $entityManager->getRepository(Stage::class)->find($id);
            $Lestage->setReferent($referent);
            $entityManager->persist($Lestage);
            $entityManager->flush();
            return $this->redirectToRoute('liste_stage');
        }

        return $this->render('stage/referent.html.twig', ['stage' => $stage,'form' => $form->createView(),]);
    }

}
