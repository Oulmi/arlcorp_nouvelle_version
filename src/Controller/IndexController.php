<?php

namespace App\Controller;


use App\Form\FormType;
use App\Entity\Eleve;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Form;


class IndexController extends AbstractController
{

    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        return $this->render('security_user/homepage.html.twig');
    }

    /** * @Route("/inscription", name= "inscription") */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $eleve = new Eleve();
        $form = $this->createForm(FormType::class, $eleve);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($eleve, $eleve->getPlainPassword());
            $eleve->setMotDePasse($password);
            $eleve->setStage("0");
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eleve);
            $entityManager->flush();
            $this->addFlash('success',
                'Connexion établie avec succès.');
            return $this->redirectToRoute('login');
        }
        return $this->render('index/inscription.html.twig',
            ['form' => $form->createView(), 'mainNavRegistration' => true,
                'title' => 'Inscription']);
    }

    //        $form = $this->createFormBuilder($eleve)
//            ->add('prenom', TextType::class, array('label' => "Prenom"))
//            ->add('nom', TextType::class, array('label' => "Nom"))
//            ->add('pseudo', TextType::class, array('label' => "Pseudo"))
//            ->add('plainpassword', PasswordType::class, array('label' => "Mot de Passe :"))
//            ->add('save', SubmitType::class, array('label' => "Sign in"))
//            ->getForm();

    /** * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, ['label' => 'Pseudo'])
            ->add('_password', PasswordType::class, ['label' => 'Mot de passe'])
            ->add('ok', SubmitType::class, ['attr' =>
                ['class' => 'btn-primary btn-block']])
            ->getForm();

        return $this->render('index/login.html.twig', ['error'=>$error,
            'form' => $form->createView(),
            'last_username' => $lastUsername,
          ]);
    }

}