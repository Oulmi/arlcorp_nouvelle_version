<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028133440 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE eleve CHANGE stage stage VARCHAR(25) NOT NULL');
        $this->addSql('ALTER TABLE entreprise CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE nom nom VARCHAR(25) NOT NULL, CHANGE ville ville VARCHAR(25) NOT NULL, CHANGE mail mail VARCHAR(27) NOT NULL, CHANGE tel tel INT NOT NULL, CHANGE active active INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stage CHANGE tuteur_id tuteur_id INT DEFAULT NULL, CHANGE entreprise_id entreprise_id INT DEFAULT NULL, CHANGE professeur_id professeur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tuteur ADD nom VARCHAR(25) DEFAULT NULL, ADD prenom VARCHAR(20) DEFAULT NULL, ADD statut VARCHAR(50) DEFAULT NULL, ADD mail VARCHAR(50) DEFAULT NULL, ADD tel INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE eleve CHANGE stage stage VARCHAR(25) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE entreprise CHANGE id id INT NOT NULL, CHANGE nom nom VARCHAR(38) NOT NULL COLLATE latin1_swedish_ci, CHANGE ville ville VARCHAR(24) NOT NULL COLLATE latin1_swedish_ci, CHANGE mail mail VARCHAR(27) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE tel tel VARCHAR(16) DEFAULT NULL COLLATE latin1_swedish_ci, CHANGE active active INT NOT NULL');
        $this->addSql('ALTER TABLE stage CHANGE tuteur_id tuteur_id INT NOT NULL, CHANGE professeur_id professeur_id INT NOT NULL, CHANGE entreprise_id entreprise_id INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP nom, DROP prenom, DROP statut, DROP mail, DROP tel');
    }
}
