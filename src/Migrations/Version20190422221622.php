<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190422221622 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE eleve CHANGE nom nom VARCHAR(25) NOT NULL, CHANGE pseudo pseudo VARCHAR(50) NOT NULL, CHANGE mot_de_passe mot_de_passe VARCHAR(64) NOT NULL, CHANGE stage stage VARCHAR(25) NOT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE entreprise CHANGE nom nom VARCHAR(25) NOT NULL, CHANGE ville ville VARCHAR(25) NOT NULL, CHANGE cp cp INT NOT NULL, CHANGE adresse adresse VARCHAR(44) NOT NULL, CHANGE mail mail VARCHAR(27) NOT NULL, CHANGE tel tel INT NOT NULL, CHANGE activite activite VARCHAR(36) NOT NULL, CHANGE active active INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stage ADD referent_id INT DEFAULT NULL, DROP professeur_id, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE tuteur_id tuteur_id INT DEFAULT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936986EC68D8 FOREIGN KEY (tuteur_id) REFERENCES tuteur (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936935E47E35 FOREIGN KEY (referent_id) REFERENCES eleve (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369A6CC7B2 FOREIGN KEY (eleve_id) REFERENCES eleve (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C27C936986EC68D8 ON stage (tuteur_id)');
        $this->addSql('CREATE INDEX IDX_C27C936935E47E35 ON stage (referent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C27C9369A6CC7B2 ON stage (eleve_id)');
        $this->addSql('CREATE INDEX IDX_C27C9369A4AEAFEA ON stage (entreprise_id)');
        $this->addSql('ALTER TABLE tuteur ADD nom VARCHAR(25) DEFAULT NULL, ADD prenom VARCHAR(20) DEFAULT NULL, ADD statut VARCHAR(50) DEFAULT NULL, ADD mail VARCHAR(50) DEFAULT NULL, ADD tel VARCHAR(10) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE eleve CHANGE nom nom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE pseudo pseudo VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mot_de_passe mot_de_passe VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE stage stage VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE roles roles VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE entreprise CHANGE nom nom VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ville ville VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE cp cp VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE adresse adresse VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE tel tel VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE activite activite VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE active active VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE stage MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936986EC68D8');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936935E47E35');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369A6CC7B2');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C9369A4AEAFEA');
        $this->addSql('DROP INDEX IDX_C27C936986EC68D8 ON stage');
        $this->addSql('DROP INDEX IDX_C27C936935E47E35 ON stage');
        $this->addSql('DROP INDEX UNIQ_C27C9369A6CC7B2 ON stage');
        $this->addSql('DROP INDEX IDX_C27C9369A4AEAFEA ON stage');
        $this->addSql('ALTER TABLE stage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE stage ADD professeur_id INT NOT NULL, DROP referent_id, CHANGE id id INT NOT NULL, CHANGE tuteur_id tuteur_id INT NOT NULL');
        $this->addSql('ALTER TABLE tuteur DROP nom, DROP prenom, DROP statut, DROP mail, DROP tel');
    }
}
