<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190421135022 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE stage (id INT AUTO_INCREMENT NOT NULL, tuteur_id INT DEFAULT NULL, professeur_id INT DEFAULT NULL, eleve_id INT DEFAULT NULL, entreprise_id INT DEFAULT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, INDEX IDX_C27C936986EC68D8 (tuteur_id), INDEX IDX_C27C9369BAB22EE9 (professeur_id), UNIQUE INDEX UNIQ_C27C9369A6CC7B2 (eleve_id), INDEX IDX_C27C9369A4AEAFEA (entreprise_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936986EC68D8 FOREIGN KEY (tuteur_id) REFERENCES tuteur (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369BAB22EE9 FOREIGN KEY (professeur_id) REFERENCES professeur (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369A6CC7B2 FOREIGN KEY (eleve_id) REFERENCES eleve (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C9369A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE eleve CHANGE nom nom VARCHAR(25) NOT NULL, CHANGE pseudo pseudo VARCHAR(50) NOT NULL, CHANGE mot_de_passe mot_de_passe VARCHAR(64) NOT NULL, CHANGE stage stage VARCHAR(25) NOT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE entreprise ADD cp INT NOT NULL, DROP code_postal, CHANGE nom nom VARCHAR(25) NOT NULL, CHANGE ville ville VARCHAR(25) NOT NULL, CHANGE adresse adresse VARCHAR(44) NOT NULL, CHANGE mail mail VARCHAR(27) NOT NULL, CHANGE tel tel INT NOT NULL, CHANGE activite activite VARCHAR(36) NOT NULL, CHANGE active active INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tuteur ADD nom VARCHAR(25) DEFAULT NULL, ADD prenom VARCHAR(20) DEFAULT NULL, ADD statut VARCHAR(50) DEFAULT NULL, ADD mail VARCHAR(50) DEFAULT NULL, ADD tel VARCHAR(10) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE stage');
        $this->addSql('ALTER TABLE eleve CHANGE nom nom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE pseudo pseudo VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mot_de_passe mot_de_passe VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE stage stage VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE roles roles VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE entreprise ADD code_postal VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, DROP cp, CHANGE nom nom VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ville ville VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE adresse adresse VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE mail mail VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE tel tel VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE activite activite VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE active active VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE tuteur DROP nom, DROP prenom, DROP statut, DROP mail, DROP tel');
    }
}
