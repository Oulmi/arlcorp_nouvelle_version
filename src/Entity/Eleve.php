<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Table(name="eleve")
 * @ORM\Entity(repositoryClass="App\Repository\EleveRepository")
 */
class Eleve implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @ORM\Column(name="prenom", type="string")
     */
    private $prenom;

    /**
     * @ORM\Column(name="nom",type="string")
     */
    private $nom;

    /**
     * @ORM\Column(name="pseudo",type="string")
     */
    private $pseudo;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string")
     */
    private $motDePasse;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ParentEleve", inversedBy="eleves")
     */
    private $parents;

    /**
     * @ORM\Column(type="string")
     */
    private $stage;

    /**
     * @var array
     * @Assert\NotBlank
     * @ORM\Column(type="array")
     */
    private $roles;


    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $plainPassword;

    public function __construct()
    {
        $this->parents = new ArrayCollection();
        //$this->roles=['ROLE_PROFESSOR'];
        $this->roles=['ROLE_USER'];
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function addRole($role)
    {
        $this->roles[]=$role;
    }

    public function setRoles(array $roles)
    {
//        if (in_array('ROLE_PROFESSOR', $roles)) {
//            $roles[] = 'ROLE_PROFESSOR';
//        }
        foreach ($roles as $role) {
            if (substr($role, 0, 5) !== 'ROLE_') {
                throw new InvalidArgumentException("Chaque rôle doit commencer par 'ROLE_'");
            }
        }
        $this->roles = $roles;
        return $this;
    }


    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUsername()
    {
        return $this->pseudo;
    }

    public function getPseudo()
    {
        return $this->pseudo;
    }


    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getSalt()

    { // you *may* need a real salt depending on your encoder // see section on salt below
        return null;
    }
    public function eraseCredentials()
    {
    }
    public function getPassword()
    {
        return $this->motDePasse;
    }

    public function getMotDePasse()
    {
        return $this->motDePasse;
    }
    public function setMotDePasse($motDePasse)
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    public function setPassword($motDePasse)
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }
    public function getParents(): Collection
    {
        return $this->parents;
    }

    public function addParent(ParentEleve $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents[] = $parent;
        }

        return $this;
    }
    /**
     * @return Collection|ParentEleve[]
     */

    public function removeParent(ParentEleve $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
        }

        return $this;
    }

    public function getStage(): ?string
    {
        return $this->stage;
    }

    public function setStage(string $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    function getPlainPassword()
    {
        return $this->plainPassword;

    }

    function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }




}
